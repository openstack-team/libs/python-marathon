python-marathon (0.13.0-7) unstable; urgency=medium

  * Switch to pybuild (Closes: #1090527).

 -- Thomas Goirand <zigo@debian.org>  Thu, 19 Dec 2024 10:05:02 +0100

python-marathon (0.13.0-6) unstable; urgency=medium

  * Add py3.12-assertEqual.patch (Closes: #1074710).

 -- Thomas Goirand <zigo@debian.org>  Sun, 14 Jul 2024 09:54:15 +0200

python-marathon (0.13.0-5) unstable; urgency=medium

  * Cleans properly (Closes: #1045153).

 -- Thomas Goirand <zigo@debian.org>  Mon, 14 Aug 2023 18:20:14 +0200

python-marathon (0.13.0-4) unstable; urgency=medium

  * Add iterable-import-from-collections.abc.patch (Closes: #1009460).

 -- Thomas Goirand <zigo@debian.org>  Sun, 01 May 2022 12:09:46 +0200

python-marathon (0.13.0-3) unstable; urgency=medium

  * Add remove-the-use-of-2to3.patch (Closes: #997524).

 -- Thomas Goirand <zigo@debian.org>  Wed, 03 Nov 2021 10:34:19 +0100

python-marathon (0.13.0-2) unstable; urgency=medium

  * Uploading to unstable.
  * Add a debian/salsa-ci.yml.
  * Fixed watch file.

 -- Thomas Goirand <zigo@debian.org>  Wed, 14 Oct 2020 12:35:43 +0200

python-marathon (0.13.0-1) experimental; urgency=medium

  * New upstream release.

 -- Thomas Goirand <zigo@debian.org>  Mon, 07 Sep 2020 15:38:01 +0200

python-marathon (0.12.0-2) unstable; urgency=medium

  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Fri, 08 May 2020 11:07:56 +0200

python-marathon (0.12.0-1) experimental; urgency=medium

  [ Ondřej Nový ]
  * Use debhelper-compat instead of debian/compat.
  * Bump Standards-Version to 4.4.1.

  [ Thomas Goirand ]
  * New upstream release.

 -- Thomas Goirand <zigo@debian.org>  Mon, 06 Apr 2020 19:17:19 +0200

python-marathon (0.11.0-2) unstable; urgency=medium

  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Tue, 16 Jul 2019 21:34:13 +0200

python-marathon (0.11.0-1) experimental; urgency=medium

  [ Ondřej Nový ]
  * d/control: Set Vcs-* to salsa.debian.org
  * d/copyright: Use https in Format
  * Removing gbp.conf, not used anymore or should be specified in the
    developers dotfiles
  * d/control: Use team+openstack@tracker.debian.org as maintainer

  [ Thomas Goirand ]
  * New upstream release.
  * Removed Python 2 support.
  * Run unit tests with installed Python module.
  * Add python3-requests-toolbelt as (build-)deps.

 -- Thomas Goirand <zigo@debian.org>  Wed, 27 Mar 2019 08:48:47 +0100

python-marathon (0.9.0-2) unstable; urgency=medium

  * Fixed debian/copyright, as per FTP master comment.

 -- Thomas Goirand <zigo@debian.org>  Wed, 01 Nov 2017 10:28:29 +0000

python-marathon (0.9.0-1) unstable; urgency=medium

  * Initial release. (Closes: #879449)

 -- Thomas Goirand <zigo@debian.org>  Sat, 21 Oct 2017 18:15:28 +0200
